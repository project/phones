<?php

namespace Drupal\phones_call\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Phones call entities.
 *
 * @ingroup phones_call
 */
class PhonesCallDeleteForm extends ContentEntityDeleteForm {


}
