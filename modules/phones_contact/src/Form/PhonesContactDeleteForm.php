<?php

namespace Drupal\phones_contact\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Phones contact entities.
 *
 * @ingroup phones_contact
 */
class PhonesContactDeleteForm extends ContentEntityDeleteForm {


}
