<?php

namespace Drupal\phones_contact\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Phones contact type entities.
 */
interface PhonesContactTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
